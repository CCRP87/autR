<?php

include_once 'adodb5/adodb.inc.php';
require_once 'adodb5/adodb-active-record.inc.php';
include_once 'config.php';

class conectarBD {

    function __construct() {
        global $CNF, $BD;
        $BD = NewADOConnection('mysql://' . $CNF->usuario . ':' . $CNF->contrasenna . '@' . $CNF->servidor . '/' . $CNF->bd);
        $BD->debug = false;
        $BD->Execute("SET NAMES 'utf8'");
        ADOdb_Active_Record::SetDatabaseAdapter($BD);
    }

}

new conectarBD();
