# LEEME #

### Sistema de autenticación  de usuarios diseñado y desarrollado especialmente para aplicaciones con API REST ###

* Es un sistema de autenticación de usuarios desarollado para sistemas con API REST o cualquier otra implementación que se le quiera.
* Version 1.0


### ¿Cómo lo configuro? ###

* El sistema requiere ADODB (está incluido)

Sólo es necesario hacer algunos ajustes en el archivo config.php que se encuentra en la raíz del proyecto, en este se realizarán las configuraciones para la conexión con la base de datos de nuestro proyecto y en la clase autR archivo(autenticacion.php), en su constructor, donde debemos asignar los valores correspondientes que señala la documentación en cada linea


### Contacto ###

* Carlos C. Romero P.