<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
        <script type="text/javascript">
            var llave;
            $(function () {
                $("#formulario-autenticacion").submit(function (e) {
                    e.preventDefault();
                    $.post('api.php', $("#formulario-autenticacion").serializeArray(), function (result) {
                        console.log(result);
                        $("#panel-autenticacion").text(JSON.stringify(result));
                        $(".llave").val(result.llave);
                    });
                });

                $("#formulario-llave").submit(function (e) {
                    e.preventDefault();
                    $.post('api.php', $("#formulario-llave").serializeArray(), function (result) {
                        console.log(result);
                        $("#panel-llave").text(JSON.stringify(result));
                    });
                });

                $("#formulario-cerrar").submit(function (e) {
                    e.preventDefault();
                    $.post('api.php', $("#formulario-cerrar").serializeArray(), function (result) {
                        console.log(result);
                        $("#panel-cerrar").text(JSON.stringify(result));
                    });
                });
            });
        </script>
    </head>
    <body>
        <fieldset>
            <legend>
                Ejemplo autenticacion con AJAX
            </legend>
            <form  id="formulario-autenticacion" name="" method="POST" enctype="multipart/form-data">
                <label>Nombre Usuario</label><br>
                <input type="text" name="nombreUsuario"/><br>
                <label>Contraseña</label><br>
                <input type="password" name="clave" /><br>
                <input type="hidden" name="accion" value="1">
                <input type="submit" value="Enviar" name="enviar" />
                <pre id="panel-autenticacion"></pre>
            </form>
        </fieldset>


        <fieldset>
            <legend>
                Ejemplo respuesta validación de llave con AJAX
            </legend>
            <form  id="formulario-llave" name="" method="POST" enctype="multipart/form-data">
                <label>Llave a consualtar</label><br>
                <input type="text" class="llave" readonly name="llave"/><br>
                <input type="hidden" name="accion" value="2">
                <input type="submit" value="Consultar" name="enviar" />
                <pre id="panel-llave"></pre>
            </form>
        </fieldset>
        <fieldset>
            <legend>
                Ejemplo respuesta cerrar la sesión con la llave con AJAX
            </legend>
            <form  id="formulario-cerrar" name="" method="POST" enctype="multipart/form-data">
                <label>Llave a consualtar</label><br>
                <input type="text" readonly class="llave" name="llave"/><br>
                <input type="hidden" name="accion" value="3">
                <input type="submit" value="Consultar" name="enviar" />
                <pre id="panel-cerrar"></pre>
            </form>
        </fieldset>

    </body>
</html>
