<?php

/*
 * En el siguiente ejemplo podemos ver la forma en que se implementa esta utilidad
 * o
 * primero se debe hacer una instacia de la clase autR 
 * apartir de ahí podemos hacer llamados a las funciones que necesitemos,
 * en el ejemplo se muestra un switch al cual le pasamos por get un parametro, 
 * en este caso un número 1, 2 ó 3 de acuerdo a lo que estemos necesitando
 * y esta nos devolverá en json el resualtado  
 * @author Carlos César Romero Pertuz
 * @copyright GPL v3
 */
require_once './autenticacion.php';

$autR = new autR();

header('Content-Type: application/json');

switch ($_POST['accion']) {
    case 1:
        $autR->consultarUsuario($_POST['nombreUsuario'], $_POST['clave']);
        break;
    case 2:
        echo $autR->verificaLlave($_POST['llave']);
        break;
    case 3:
        $veriificar = json_decode($autR->verificaLlave($_POST['llave']));
        if (!$veriificar->estado) {
            echo json_encode($veriificar);
        } else {
            $autR->cerrarSesion($_POST['llave']);
        }
        break;
    default:
        echo json_encode(array('mensaje'=>'la función solicitada no existe','estado'=>FALSE));
        break;
}