<?php

include_once 'lib/conexionBD.php';
include_once 'modelos/modeloautenticacion.php';


/*
 * autR es una clase que nos facilitará el tema de la autenticacion de los 
 * usuarios en nuestras aplicaciones dado que esta cuanta con las funciones 
 * necesarias para manejar este tema.
 * 
 * Para hacer uso de esta solo debemos tener en nuestra tabla de autenticación 
 * 5 columnas destinadas para amacenar los siguientes datos: 
 * id de la tabla, nombre del usuario, contraseña o clave, una llave, ultimo acceso
 * las cuales deben corresponder a los siguientes tipos de datos: 
 * entero, varchar, vachar, varchar, entero
 * @author Carlos César Romero Pertuz
 * @copyright GPL v3
 */

class autR {

    var $nombreTabla;
    var $colIdTabla;
    var $colNombreUsuario;
    var $colClave;
    var $colLlave;
    var $colUltimoAcceso;
    var $tiempoSesion;

    function __construct() {
        $this->nombreTabla = 'usuario'; //Nombre de la tabla de la base de datos donde se hará la autenticacion
        $this->colIdTabla = 'usuarioid'; //Nombre la columna de la llave primaria de la tabla
        $this->colNombreUsuario = 'usuarionombre'; //Nombre la columna de la destinada para almacenar el nombre del usuario
        $this->colClave = 'usuarioclave'; //Nombre la columna de la destinada para almacenar la contraseña o clave del usuario
        $this->colLlave = 'usuariollave'; //Nombre la columna de la destinada para almacenar una llave md5
        $this->colUltimoAcceso = 'ultimoacceso'; //Nombre la columna de la destinada para almacenar la fecha del ultimo acceso en formato unixtime
        $this->tiempoSesion = 60;  // tiempo que se estima o se desee que dure la sesión del usuario (segundos)
    }

    /*
     * Esta función permite validar que un usuario esté en nuestro sistema
     * 
     * @param string $usuarioNombre Nombre del usuario del sistema
     * @param string $usuarioClave Contraseña 0 clave del usuario 
     * @return json contiene la llave de la sesión del usuario (se genera en cada acceso) 
     * o mensaje en caso de autenticacion fallida y el estado (falso o verdadero)
     * 
     */

    function consultarUsuario($usuarioNombre, $usuarioClave) {

        if (empty($usuarioNombre)) {
            die(json_encode(array('mensaje' => 'Error, falta un parámetro requerrido', 'estado' => FALSE)));
        }
        if (empty($usuarioClave)) {
            die(json_encode(array('mensaje' => 'Error, falta un parámetro requerrido', 'estado' => FALSE)));
        }

        $usuarioModel = new modeloAuthR($this->nombreTabla);

        $resultado = $usuarioModel->Find($this->colNombreUsuario . '=? and ' . $this->colClave . '=?', array($usuarioNombre, $usuarioClave));
        if (count($resultado) > 0) {
            $usuario = new modeloAuthR($this->nombreTabla);
            $usuario->Load($this->colIdTabla . '=?', array($resultado[0]->{$this->colIdTabla}));
            $usuario->{$this->colLlave} = md5($resultado[0]->{$this->colIdTabla}.rand(). time());
            $usuario->{$this->colUltimoAcceso} = time();
            if ($usuario->Save()) {
                echo json_encode(array('llave' => $usuario->{$this->colLlave}, 'estado' => TRUE));
            } else {
                echo json_encode(array('mensaje' => 'No se pudo actualizar la llave', 'estado' => FALSE));
            }
        } else {
            echo json_encode(array('mensaje' => 'Credenciales incorectas', 'estado' => FALSE));
        }
        unset($resultado, $usuario, $usuarioModel);
    }

    /*
     * Esta función debe ser llamada en cada peticion que el usuario realice al sistema 
     * con el fin de validar la validéz de la peticion y actualizar el ultimo acceso 
     * el cual determinará si la sesión ha caducado
     * 
     * @param string $llave generada al momento de la autenticación 
     * @return json Mensaje y estado de la peticion  
     * 
     */

    function verificaLlave($llave) {
        if (empty($llave)) {
            return json_encode(array('mensaje' => 'Error, falta un parámetro requerrido.', 'estado' => FALSE));
        }
        $usuarioModel = new modeloAuthR($this->nombreTabla);
        $resultado = $usuarioModel->Find($this->colLlave . '=?', array($llave));
        if (count($resultado) > 0) {
            $usuario = new modeloAuthR($this->nombreTabla);
            $usuario->Load($this->colIdTabla . '=?', array($resultado[0]->{$this->colIdTabla}));
            if ((time() - $resultado[0]->{$this->colUltimoAcceso}) > $this->tiempoSesion) {
                return json_encode(array('mensaje' => 'Su llave de autenticación a caducado.', 'estado' => FALSE));
            } else {
                $usuario->{$this->colUltimoAcceso} = time();
                $usuario->Save();
                return json_encode(array('mensaje' => 'Su llave de autenticación es valida.', 'estado' => TRUE));
            }
        } else {
            return json_encode(array('mensaje' => 'Su llave de autenticación no es valida.', 'estado' => FALSE));
        }
        unset($usuarioModel, $usuario, $resultado);
    }

    /*
     * Esta función debe ser llamada cuado el usuario decida salir del sistema
     * de lo contrario la sesión seguirá abierta hasta que el tiempo de la caduque la llave
     * 
     * @param string $llave generada al momento de la autenticación 
     * @return json Mensaje y estado de la peticion
     * 
     */

    function cerrarSesion($llave) {
        if (empty($llave)) {
            die(json_encode(array('mensaje' => 'Error, falta un parámetro requerrido', 'estado' => FALSE)));
        }

        $usuarioModel = new modeloAuthR($this->nombreTabla);
        $resultado = $usuarioModel->Find($this->colLlave . '=?', array($llave));
        if (count($resultado) > 0) {
            $usuarioModel = new modeloAuthR($this->nombreTabla);
            $usuarioModel->Load($this->colIdTabla . '=?', array($resultado[0]->{$this->colIdTabla}));
            $usuarioModel->{$this->colLlave} = md5(rand(0, time()));
            if ($usuarioModel->Save()) {
                echo json_encode(array('mensaje' => 'Se ha cerrado la sesión con exito', 'estado' => TRUE));
            } else {
                echo json_encode(array('mensaje' => 'Lo sentimos, No se pudo cerrar la sesión', 'estado' => FALSE));
            }
        } else {
            echo json_encode(array('mensaje' => 'Su llave de autenticación no es valida.', 'estado' => FALSE));
        }
        unset($usuarioModel, $resultado);
    }

}
